
# Simple implementation of Continuous  Integration for Python with Gitlab

Objective: Show how to implement continous integration in a Python project.

Description:  transform a simple calculator class into a python project a  CI mechanism.

## Tooling choices

GITLAB:

- For the purpose I use Gitlab,  it offers both a versionning system as well as CI suite tools.

PYTEST & PYLINT :

- For testing  the code => Pytest is the standard
I like in particular because of it
"test coverage"
- For linting ist check with pylint

## Implentation

For a some expalaination  on CI concepts please read the  Real Python tutorials on the topic.
My implementation follow the same principles only the tools choices is different.

## How to install  and test the workflow

```shell
        - git clone https://gitlab.com/remitoudic/ci_cd_test.git
        - For setting a virtual enviromnent check Prasth tool README.md
        - For test_coverage cli =>  pytest -v --cov
        - For test_linting  cli =>  pylint calculator.py
```

reference:
<https://realpython.com/python-continuous-integration/>
